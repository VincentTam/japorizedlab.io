// Inspired by Eduardo Boucas (@eduardoboucas)
document.querySelector(".loadMore").addEventListener('click', fetchMorePosts);

function fetchMorePosts() {
  var _this = this,
      $blogContainer = document.getElementById('blog_container'),
      nextPage = parseInt($blogContainer.getAttribute("data-curPage")) + 1,
      totalPages = parseInt($blogContainer.getAttribute("data-totalPages"));

  _this.classList.add('loading');
  _this.innerHTML = ". . .";

  var pathArr = location.href.split('/'),
      directory = pathArr[3];
  
  AJAXget('/' + directory + '/page' + nextPage + '/',
    function(data) {
      window.setTimeout(function() {
        function parseHTML(str) {
              var tmp = document.implementation.createHTMLDocument();
              tmp.body.innerHTML = str;
              return tmp.body.children;
            };
        var doc = parseHTML(data),
            posts = doc.namedItem('blog-outer-container').querySelectorAll('article.masonry-item'),
            articleFrags = document.createDocumentFragment();

        $blogContainer.setAttribute("data-curPage", nextPage);

        posts.forEach(function(article){
          var a = article;
          a.style = "";
          a.classList.add('msnry-animate');
          articleFrags.appendChild(a);
        });

        var postMsnry = document.querySelector('.post-masonry');
        postMsnry.appendChild(articleFrags);
        msnry.recalculate();
        postMsnry.querySelectorAll('.msnry-animate').forEach(function(article) {
          article.classList.remove('msnry-animate');
        });

        if (totalPages == nextPage) {
          var loader = document.querySelector('.loadMore');
          loader.parentNode.removeChild(loader);
        } else {
          _this.innerHTML = "Load more posts";
          _this.classList.remove('loading');
        }
      }, 300);
    },
    function(xhr) {
      console.log(xhr);
    }
  );
}
