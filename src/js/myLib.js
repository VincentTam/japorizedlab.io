// Object "length"
Object.size = function(obj) {
  return Object.keys(obj).length;
}

// Type of Object
function type(obj) {
    return Object.prototype.toString.call(obj).replace(/^\[object (.+)\]$/, '$1').toLowerCase();
}

/*
 * GET request using AJAX for JSON
 */
function getJSON(path) {
  return new Promise(function(resolve, reject) {
    var xhr = new XMLHttpRequest();
    xhr.open("GET", path, true);
    xhr.responseType = 'json';
    xhr.onload = function() {
        if (xhr.readyState === XMLHttpRequest.DONE) {
            if (xhr.status === 200) {
                resolve(xhr.response);
            } else {
              reject(xhr.status);
            }
        }
    };
    xhr.send();
  });
}

/* 
 * GET request using AJAX
 */
function AJAXget(path, success, error) {
  var xhr = new XMLHttpRequest();
  xhr.open('GET', path, true);
  xhr.onload = function() {
    if ( xhr.status >= 200 && xhr.status < 400 ) {
      if (success)
        success(xhr.responseText);
    } else {
      console.error("Target server " + path + " was reached but an error was returned.");
    }
  };
  xhr.onerror = function() {
    error(xhr);
  };
  xhr.send();
}

/*
 * Object.has($el)
 * ===
 * returns Boolean
 * - $el : element
 */
Object.has = function(self, el) {
  if (String(el.classList) != "") {
    if (self.querySelector('.' + String(el.classList).replace(/\s/g, '.') ) != null) {
      return true;
    } else {
      return false;
    }
  } else if (el.id != '') {
    if (self.querySelector('#' + el.id) != null) {
      return true;
    } else {
      return false;
    }
  } else {
    if (self.querySelector(el.tagName) != null) {
      return true;
    } else {
      return false;
    }
  }
}
