---
layout: projects
type: catalog
title: projects
projects: 
  - title: TeX Notes
    url: https://tex.japorized.ink
    img: tex_notes.jpg
    description: My workflow for LaTeX. Repo is online, with a front page (linked).
  - title: enpitsu
    url: https://gitlab.com/japorized/enpitsu
    img: enpitsu.jpg
    description: (WIP) A terminal todo manager written in Javascript
  - title: elementary
    url: https://japorized.gitlab.io/elementary
    img: elementary.jpg
    description: A simple personal startpage
  - title: vim-template
    url: https://gitlab.com/japorized/vim-template
    img: vim-template.jpg
    description: A vim plugin that provides a very basic templating capability to vim. Optionally dependent on Denite.nvim.
---

## Projects and Showcases
