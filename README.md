# Japorized | japorized.gitlab.io 

### Special thanks to these sources
* [FontAwesome](http://fontawesome.io) for awesome icons
* Eduardo Boucas for the inspiration on Ajaxifying the Blog Index Page.
* [KaTeX](https://katex.org/) for parsing LaTeX in HTML
* [Macy.js](http://macyjs.com/) for the minimal masonry layout.
* [Lunr.js](https://lunrjs.com) for the quick static site search, right from the client's side. Thanks [RayHighTower](http://rayhightower.com/blog/2016/01/04/how-to-make-lunrjs-jekyll-work-together/) for the guide.

### TODO
* Use [Fuse.js](https://fusejs.io/) for fuzzy search, effectively replacing old Lunr.js
* See if [instant.page](https://instant.page/) is helpful
