---
layout: post
type: post
title: Site Makeover - Let's go Tufte style!
description: Short post about what has changed for this site, reasons behind them, and the difficulties that I faced.
title-img: 
title-img-caption: 
category: siteupdate
tags: [ 'web-design', 'bootstrap' ]
comments-enabled: true
---

Granted, this is a pretty late post for something that I have completed a month ago now, but let's see what I can cover.

I will be honest and come out saying that the last design was basically not my design at all, but rather a stepping stone
for myself to get familiar with the new landscape of web designing and web technologies. It was the standard Bootstrap-y
layout/theme, one that has become the _de facto_ standard design of the web, one that has recently become a design chided
by those that are familiar as the style of the lazy, or the style of business sites.

To be fair, I liked the clean, clear but bold design, one that presents information to readers in a clear manner, allowing
them to sit back and read at the comfort of their chairs, staying a healthy distance from their screens. On the other hand,
while I liked the idea of centering the main content, and apply it even to the text alignment itself, it quickly become 
irritating in certain posts that aim to be more informative. Fully-centered text has its appeal, especially when sentences
are succinct, or when one tries to present, say a poem, but it certainly should not be for everything.

The Bootstrap way of a website was not designed for blogs, or, more generally, writing. It is a means of presenting short
information, accompanied with images or videos, to _provide less at every scroll for our brains to absorb the presented
information more easily so_.

So yes for most product sites, business or organization homepages, and prototyping. Not so much for math and writing.

---

## Enter Tufte style

The goal of the remake is to keep the good things that I liked and wanted from the last one, and taking away the weaknesses
that I outlined. One of the stronger motivations of the new design does come from the well-celebrated 
[Tufte style](http://www.edwardtufte.com/tufte/books_be), which is the main style that I go with 
[my documents](https://tex.japorized.ink) these days. That is, to have wide margins on the right side of a page, so as to
allow for the author to insert notes, either with an anchor to the main text {% sidenote 'sn-sidenote' 'This is a sidenote.' %},
or simply on its own{% marginnote 'mn-marginnote' 'This is a marginnote.' %}. Instead of having the reader scroll all the way
down to the end of the post for footnotes and end up losing track of where they were, side notes and margin notes serve the
same purpose but stays out of the readers way at the same time.

{% marginnote 'mn-notes-management' 'Incidentally, the Tufte style of documents is also how I handwrite my notes, since before
I came to know that it is a style of its own.' %}
The large margin space also serves an indirect purpose of keeping the length of each line on the main text shorter, allowing
the reader to skim faster without going onto the wrong line.

---

## Forgoing CSS Frameworks

I have decided to drop the usage of CSS frameworks through this revamp, with the main reason being that most of what I have
found turns out to get in the way and provide many stylings that are unnecessary for what I am going for.

CSS frameworks are great, there is nothing to argue about them being the useful scaffolding that they are. But their styles are
opinionated, just as design of this site is also opinionated. Certainly, I can simply override the _default_ styles from the 
frameworks with my own, but that is no reason for me to make the user download more kilobytes of styles that will never be 
parsed and used by their browsers. This reasoning does not scale for growing projects, but this site is not going to scale.

Perhaps this calls for an unopinionated, bare-boned CSS _framework_{% sidenote 'sn-css-library' 'Instead of a framework,
this idea is more like a library.' %} that provides only classes to managing responsiveness.

That said, the challenge to working without a well-written, well-tested CSS framework is that I now have to manage the
responsiveness of the site on my own. As of now, the responsiveness is somewhat hard-coded in the CSS file, in that the widths
are class- or id-specific, instead of a general class to apply the responsive properties to elements that have the class. But
as pointed out above, perhaps a generic CSS flexbox/grid solution would end this problem.

---

## Dark mode

Needless to say that you have probably noticed that the site has a much brighter theme: one that has a light smell of good
ol' paperback, at least that is what you would see in the day. I usually do not enjoy brightly colored websites, and would go
out of my way to either avoid these sites, or apply a CSS override to make them dark. I am perfectly fine with a bright website,
however, when it is actually bright out and not close to bedtime. And so I wrote a simple JS script to apply the light and dark
themes at different times of the day, depending solely on your system time.

The challenge here is not so much on the user experience of how the theme is applied or changed, but on the developer's sanity.
CSS is not a dynamic language; it is mostly static. Styles are applied on top of one another with specific ways of giving
priority to a style{% sidenote 'sn-css-priority' 'Some factors include which css file was included later, which selector is more
specific, etc.' %}, and the only way CSS can be _replaced_ after being loaded is via Javascript, which turns into a giant spaghetti
monster if there are many CSS styles to replace.

So dynamically switching each style using Javascript is out of the window. The only other option, as of now, is to either load a
separate CSS file after the first, or simply replace the first through overriding styles that are different in dark mode.

Now the first seemingly equates itself to insanity: I have to maintain two separate CSS files of the exact same styles but
different colors. Indeed, in pure, browser-compatible CSS{% sidenote 'sn-css-variables' 
'<a href="https://caniuse.com/#feat=css-variables">CSS Variables</a> may be a thing, but Edge users and Opera users would not be
able to make use of it appropriately. But, hopefully, with Microsoft 
<a href="https://www.techradar.com/news/microsoft-confirms-edge-is-moving-to-chromium">moving over to a Chromium-based browser</a>,
this problem will become less of a headache. That said, I do not like the fact that Chrome/Chromium is now going to have an even
larger market share on the browser market.' %}, this is a nightmare for the developer, as it{% sidenote 'sn-why-it' 'I am starting
to prefer calling a person by <i>it</i> instead of <i>he/she</i>, not because of gender neutrality reasons, but simply because I am
lazy and the latter does not roll off the tongue too well.' %} would have to keep the two CSS files _somehow_ in sync while making
sure that new styles are reflected in both the files, especially if one has a specific styling depending on the style being on light
background or dark background.

A _rather_ natural _solution_ would be to use the second method of overriding the default style{% sidenote 'sn-light-default' 'Let
us suppose that the default theme is the light theme.' %} with the new style, by simply having the styles that are different in
dark mode be in a separate file, and have that load when we require dark mode. But the above problem still persists, being a lot
less of an annoyance, but still an annoyance that scales as the CSS file itself gets larger.

These were challenges faced by web devs in the past, one that we did not have a good way to solving then. But now, it is a much
easier problem to solve, by combining the power of CSS preprocessors and task runners.

Note that the goal is to be as browser-<s>dumb</s>*friendly* as possible, so we will avoid relying on native CSS variables and
importing, for now.

I created two different SCSS files to store my color variables, one that provides for the light theme, and the other for dark. Then,
use a unique name for each file, and unique in the sense that it is unique from all other strings that you will find in whichever
SCSS file that you will be importing them. For example, I named my files as `_colorlight.scss` and `_colordark.scss`.

Now in my main SCSS file, I imported only one of the files, which is the only main CSS file that I write. To automatically create
a separate CSS file for dark mode, I wrote an almost-one-line bash script to replace the imported filename in the main SCSS file.
```bash
#!/usr/bin/env bash

FILE=$1
DEST=$2

sed 's/\"colorlight\"/\"colordark\"/g' $FILE > $DEST
```
The double quotes are certainly unnecessary, but I included it nonetheless to further narrow down my choices, so as to allow for the
possibility of me using a similar class name later on.

Following that, I added a new gulp task in my `gulpfile.js`:
```js
gulp.task('dark-theme', (done) => {
  const darkConv = child.spawn('scripts/dark-mimic', [
    'src/css/style.scss',
    'src/css/night.scss'
  ], {stdio: 'inherit'});
  done();
});
```
Also, I added an extra watcher to the main SCSS file:
```js
gulp.task('watch', () => {
  gulp.watch(['src/css/*.scss', 'src/css/*.sass'], gulp.series('sass'));
  gulp.watch('src/js/**/*.js', gulp.series('javascript'));
  gulp.watch('src/css/style.scss', gulp.series('dark-theme'));
});
```
This ensures that whenever I update my main SCSS file when my main development task is running, it will create a copy of the main
file with the imported color changed, and the `sass` task kicks in and processes both SCSS files into CSS.

<span class="newthought">No doubt</span>, some users would probably want to be able to switch between light and dark
mode as they
please. And yes they certainly can!
{% include video.html src='site-theme-change.mp4' caption='Theme changer!' %}

~~The problem now would be that the theme-change is not persistent. In particular, when the user switches to another theme, the change
applies only to the current view, and the change is lost every time the user navigates away from the current view (e.g., by clicking
a link). The current and only way to solve this now is to introduce coookies, which have become somewhat of a taboo these days. I
have put this feature to the back burner for now, trying to find a solution that does not involve cookies, if there is one at all,
but it should be impossible given the current state of things.~~

Edit on May 25th: [HTML5 Web Storage](https://www.w3schools.com/html/html5_webstorage.asp) is a thing! This is now easy peasy, aside from support for really old browsers.
