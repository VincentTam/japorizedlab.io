---
layout: post
type: post
title: Take A Step Back
description: Social media and platforms have completely taken over the lives of millions. It's time to take a step back and reevaluate how it has impacted our lives.
title-img: 
title-img-caption: 
category: argumentative
tags: [ ]
comments-enabled: true
---

To the young, how often have you heard the following?

> Yes! I've got x likes!

> Hey, why didn't you like my photo on Instagram?

> Have you seen my story on Snapchat? WHAT? You don't have Snapchat?

See the common theme that's running there? It's all about "me". It is not an overstatement to say that most social platforms today are basically platforms for people to compete and compare their ego, inflating them to disproportionate sizes.

It may be the case, in fact, it probably was the case, that people wanted to use social media as a medium to share their experiences, their memories, and their happiness. But along the way, that pure intent has been muddled with the desire for attention, making it difficult for some (or many) to tell the two apart.

Plus, take another look at those statements; don't you find it ridiculous? *Why oh why are we talking about likes on a selfie taken in your room? Why should I keep up with watching what you've ate on your Snapchat stories? What if you're not the only one who wants me to do that?*

5 months ago, as I noticed that I have not been on most of the social platforms that I am on, as I see the number of notifications, most of which have almost nothing to do with me or anything that I care, pile up, I decided to put an end to that ridiculousness.

Social platforms used to be a means to keep people connected. Now, it's just a place where people dump their daily activities, basically keeping a public diary about their everyday lives.

##### Mind you, you're not making or maintaining connections through all those posts.

I don't want to know how aesthetically pleasing was your dinner. I don't want to know that you've just finished your daily workout through your sweaty face on your recent selfie. I don't want to read about you indirectly throwing shade at someone who treated you badly today, and yet again through a pointless selfie.

**Share with me** about what you've learnt today. **Tell me why** you're celebrating over a nice dinner. **Tell me about** your take on personal health. **Tell me** about how difficult it is seeing your sickly elders suffer. **Tell me** about how to handle difficult people. **Tell me** about how confused or lost you are in our crazy world today.

##### Kill that camera app you're using and come talk to me.
