---
layout: post
type: post
title: Making the move to Linux
description: In this post, I share about why I decided to make a move to using a Linux-based system, in particular, the Arch-based Antergos OS, in which I use it almost like a pure Arch Linux system. I will also share a couple of features in my setup.
title-img: systemscrot.png
title-img-caption: Faking busy 🎶
category: technical
tags: [ 'operating-system', 'archlinux', 'linux', 'macos' ]
comments-enabled: true
---

Before criticism ensues, let it be known that I love the macOS. I have used macOS for almost 8 years, on two 13-inch MacBook Pro's. I love how I am free from the nightmares that I have had, as a much younger child, with Windows; I love that I have Retina Display; I love that I have a system that looks nicer than almost every other Window system that I have seen and come across; I love how I have, which I have come to learn later on, a UNIX-like environment, which enabled me to, and later on I did, learn how to work comfortably in a shell. As I came to own an iPhone, I enjoyed having iTunes and iCloud sync across my devices; no longer do I have to manually move music files into an SD Card for my phone, no longer do I think about leaving important notes on my computer while not being able to access them anywhere else (I have never owned an Android device, and I loathe the day that I have to use one).

While we're still on that topic, here's a big shout out to Alfred. Thank you for the services that you have provided for me all these years. Alfred has enabled me to have very convenient app launching for many years, as my quick access to a spelling corrector and dictionary. While I do not have Powerpack purchased, it has given me so much convenience and boost in productivity, and I have yet to find a decent enough replacement for it on either Windows or Linux. Alfred is lightning fast, accurate, and has a reliable fuzzy finder; all that you cannot find in any of those that claim to be alternatives or replacements.

My current laptop is a mid-2014 model, which means I have escaped most of the recent nightmares with newer models of the product line. So that is not why I am making a switch; and yes I am now using Antergos on my MacBook Pro, typing this post on Antergos, on a MacBook Pro, instead of being in macOS. Note that I am still a student, without a proper income, and suffering from mental health issues. This machine is currently in its 3rd year, and given my circumstances, I believe that it would be smart of me **to try all that I can to extend the life of this machine for as much as I can, so that I do not have to make any purchase of a new machine before I graduate**.

{% include image.html src="scarynumber.png" caption="More than 300 processes post-boot with only a Kitty Terminal running" %}

### That number scared me.

---

Not too long ago, I shared about my [rice]({% post_url 2018-01-24-ricing-macos %}) on macOS. It was almost a fully customized desktop experience aside from me still using native applications from Apple, despite the fact that I do not actually have as many applications running to have that experience. But before this, in recent months, or perhaps for more than a year now, there is one task that will almost always hog my virtual memory to crazy amounts and make the fan run like a dog, the `kernel_task`. I find that baffling; online searches for months returned me with no reliable results on how to "cure" this, not even a single piece of useful advice regarding the issue. Questions that relate to the issue are either replied with skepticism, or that the original poster just stop replying altogether, making the thread a hanging one.

At the same time, I have been eyeing at Arch Linux for a long time, and even had a virtual machine running the OS that I use for fun and experiments, using, at that time, awesomewm with the powerarrow theme from [awesome-copycats](https://github.com/lcpz/awesome-copycats). I had yet to start any sort of ricing at that time, simply enjoying the experience that the theme and the window manager (wm) can provide, learning scripts here and there, and collecting inspiration about what to do for my own system. The customizablity of Linux is not so much of a pull factor in my switch, given my abilities and freedom from the setup that I have on macOS.

The most incredible thing that I have come to learn while using the virtual machine was how little was the resources that the system was taking, even after factoring out the fact that it is running in a virtual environment.

Of course, that is one of the selling points of Arch Linux: a minimal distribution system; where the user has to build upon on his/her own accord, tayloring to their own needs in their own way. And this selling point has the current me sold, as it meets my current needs.

---

### And so I installed Antergos

I initially wanted to install Arch. But given the MacBook Pro's hardware, especially the network card, it was a tough choice. I tried tethering with my iPhone to at least install the right network driver for the live system, but it somehow failed to detect my phone, and being in a place where I do not have a cable for myself, I had no means of accessing the Internet, i.e. I could not install any packages from their repositories.

Not having another handy USB stick for another live system that could perhaps get my network driver to work for a chroot (I read that the chroot on macOS works differently than that on Linux and so I did not try that), I went ahead and gave Antergos a try. Lo and behold, it found my iPhone and I was able to tether.

I immediately went for the base installation which puts me in a position similar to how most would have when they install Arch, with a few extra packages (and networkmanager lurking somewhere {% sidenote 'sn-id-networkmanager' 'I am using netctl and netctl-auto, and netctl comes with the standard Arch installation.' %} 😆). Having my dotfiles fully managed meant that the time that I need to set up, at least, my terminal workspace was relatively painless, easy and quick. I was quickly able to have the same setup that I have with zsh, vim, tmux, mpd, w3m, etc.

Then comes the question of choosing a graphical display. I could go with awesomewm again and just grab the configs from my virtual machine, which is also in my dotfiles. But I wouldn't be able to learn as much and the design is not truly one that I would call my own. I certainly do not use everything that the guys over at awesome-copycats has made, and I would want to utilize the extra resolution that I have "gained". I thought about giving i3-gaps another go, since I have set up one before on a Windows machine on the WSL. But I wanted something lightweight.

It was then that I recalled that chunkwm was inspired by bspwm.

So I looked into bspwm and found that the two wms, despite on different platforms, and definitely written very differently, are almost identical to the end user. As a wm itself, it does not have hotkeys to do anything, and you would need a separate package, a hotkey daemon, to bind keys for executing commands from bspwm, which the recommended one was sxhkd. The setup of bspwm + sxhkd had a very striking resemblance to chunkwm + khd, and that really caught my attention ([bspwm](https://github.com/baskerville/bspwm) and [sxhkd](https://github.com/baskerville/sxhkd) are developed by the same developer).

Best of all, bspwm is relatively lightweight.

---

### Now for the rice

Well, it is not fully riced yet, as I am slowly adding/removing/tinkering with things on my desktop every now and then or when the need arises. Screenshots that you see in this post are the current look of my desktop as I finish typing this post. Here's a quick screencast to show how things are for me.

{% include video.html src="showoff.mp4" caption="Oh yes, I can even do screencasting!" %}

Oh, I failed to record that the computer status bar that you see in the first image in this post only shows when I hit a key combination, and the fancy looking volume, backlight, and keyboard backlight control indicators that I have set up. My setup is almost identical to this post from [/r/unixporn](https://www.reddit.com/r/unixporn/comments/6g5vx0/windowchef_comfy/) {% sidenote 'sn-id-unixporn' 'This subreddit is completely safe for work, despite the name!' %}, except that I use a dark theme, have the indicators positioned a little differently, have more info about my computer and an indicator for the workspace that I am on.

The run dialog, the would bear some resemblance to Alfred for those that are familiar with it, is [rofi](https://github.com/DaveDavenport/rofi), with an edited look to suit my color palette. You will also notice that I have a headless-like pdf reader, and that is [zathura](https://en.wikipedia.org/wiki/Zathura_(document_viewer)).

And that is about how much that I have now, which is, in my opinion, not a lot going on. Here's a screenshot of how much resource my system is currently using. Note that I have Firefox open with 6 tabs, 2 zathura instances, and 3 termite instances all with tmux running.

{% include image.html src="htop.png" caption="Much better! 😃" %}

---

### But wait! What about macOS!?

It is in a separate partition on the same disk now, and I have ripped out a lot of the things that I no longer need, but kept most of my setup intact. I have also made a third partition for storage, which the two systems can share and that I, as a user, would retain ownership on both systems {% sidenote 'sn-id-shared-partition' "Refer to <a href=\"https://lifehacker.com/5702815/the-complete-guide-to-sharing-your-data-across-multiple-operating-systems\">this article</a> for info." %}. I still prefer using Affinity Designer over the tools that are on Linux (e.g. Gravit) for vector drawing, for one. And with this kind of a setup, I can still sync music to my phone via iTunes {% sidenote 'sn-id-itunes-iphone-macos' "Although I do have to boot into macOS, but that's not too much of a pain." %}. In fact, I can probably even continue to maintain and write posts for this site on macOS!

---

### Experience so far

I have been using the system for almost 2 weeks now and I am happy that I do not feel completely foreign to it at the slightest. The only thing that I miss is Alfred's ability to quickly let me find definitions and check for spellings, which I believe I can try to poke on [goldendict](https://wiki.archlinux.org/index.php/Goldendict) for a bit once the downloadable dictionaries on the [AUR](https://wiki.archlinux.org/index.php/Arch_User_Repository) have been updated {% sidenote 'sn-id-goldendict' "I want to manage them using pacman." %}, and see if I can to hack a rofi plugin for myself.

---

##### References

1. My [dotfiles](https://github.com/japorized/dotfiles)
2. Installation Guide of Arch for MacBook Pro on [Arch Wiki](https://wiki.archlinux.org/index.php/MacBookPro11,x)
