---
layout: post
type: post
title: Site Update 2
description: 
title-img: 
title-img-caption: 
category: siteupdate
tags: [ ]
comments-enabled: true
---

Here's to my little celebration over my various <strike>achievements</strike> efforts in bringing nice little updates to my site. :tada:  <strike>All this while I further procrastinate from actually writing anything.</strike>

And so by MY will, I said,

## Let there be MATHEMATICS!

With that, $$\LaTeX$$ in the form of MathJax has been implemented.

## Let there be EMOJI!

And with the move of a couple of fingers :open_hands:, the site has been blessed with :smile: and :joy:, with thanks to JEmoji.

## Let there be LESS page REFRESHES when thy people read!

A fresh blog index design is born, and no longer shall there be page refreshes while digging for older blog posts.

## May the author live longer by working lesser!

And as such I have been blessed with two new scripts that serve to create and generate new posts and categories without the need to leave the Terminal. A better YouTube embededment has also been implemented, that is shall be responsive to the viewport.

While the above summary is not much, much had been done behind the scenes that shall not be listed here. Feel free to hop onto the site repo to look through the changes.
