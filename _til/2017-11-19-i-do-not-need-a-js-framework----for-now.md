---
layout: post
type: til
title: I do not need a JS Framework -- for now
tags: ['webdev', 'js']
comments-enabled: true
---

I have been programming on the web for about a quarter of my life, as a hobby, without actually being too serious about it until rather recently. Of course I could not stand all the buzz around web development today, but the tools that have been developed for the past 5 years or so have come a lot to my attention. So I spent most of today reading, articles after articles of JS frameworks, to try to understand the workings behind them and how I can use them for myself, as a hobbyist programmer on the web. The biggest takeaway that I can get from all the reading, and after giving React a little hands-on, is that I still do not need a Javascript Framework.

Note that I am an ancient dinosaur in terms of my knowledge, my workflow, and the tools I use.

---

## Why?

The biggest difference that sets aside my needs and what these tools provide is that I am not building web applications for now but mainly static content. I can see how React can definitely scale as promised, since web apps built using it essentially have JS wrapped entirely around them, allowing for quick dynamic, contents to be delivered to the users. Vue.js takes on a philosophy that is more similar to the traditional way (which is what I have picked up in as I learned to program on the web), that HTML is still the main file while JS supports it by providing functionality.

In a sense, personally, the traditional way of web development (at least on the front end) is having HTML as the skeleton and JS as the organs. With React, it feels as if HTML has become perhaps only the spine while JS takes over almost everything else. The skin to everything is still CSS, but JS has slowly been taking over some of the layers of that.

## "You do front-end web development? What framework do you use?"

That is a question that I have been asked too many times, and that I did not have an answer to. That is partly cause of my lack of knowledge about them. I do web development as hobby, and I have been putting more time into my studies, and none of them is related to web development, or even just programming in general. But now I can say for certain that I do not need to use one for what I am doing. It has also made me realize that my hobby is more on the UI side of things, a portion of the UX pie, and not entirely since functionality somehow does not interest me as much. If one argues that you can't do both without functionality, I do agree.

---

# References

Here is the main article that I read, and I've read quite a number of its linked articles, sites, forums, or discussions.

[Angular vs. React vs. Vue: A 2017 comparison](https://medium.com/unicorn-supplies/angular-vs-react-vs-vue-a-2017-comparison-c5c52d620176){:target="_blank"}


