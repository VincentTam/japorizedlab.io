const gulp = require('gulp'),
      child = require('child_process'),
      pump = require('pump'),
      gulpUglify = require('gulp-uglify'),
      babel = require('gulp-babel'),
      babelMinify = require('gulp-babel-minify'),
      stylus = require('gulp-stylus'),
      log = require('fancy-log'),
      htmlmin = require('gulp-htmlmin'),
      postcss = require('gulp-postcss'),
      autoprefixer = require('autoprefixer'),
      cssnano = require('cssnano');

const SITE_ROOT = "_site/",
      ASSET_IMG = "assets/img/",
      JS_SRC = "src/js/**/*.js",
      ASSET_JS = "assets/js/",
      JEKYLL_FILES = [
        '*.html',
        '_layouts/*.html',
        '_includes/**/*.html',
        '_math/*',
        '_posts/*',
        '_til/*',
        'about/*.html',
        'blog/*.html',
        'categories/*.html',
        'tags/*.html',
        'cv/*.html',
        'labs/**/*.html',
        'math/**/*',
        'til/*.html'
      ],
     BROWSER = "/usr/bin/firefox";
  
var messages = {
  jekyllBuild: '<span style="color: grey">Running:</span> $ jekyll build'
}

// Commonly repeated jobs
gulp.task('generateTags', (done) => {
  const genTags = child.spawn('scripts/generate-tags', [''], {stdio: 'inherit'}),
        genCats = child.spawn('scripts/generate-categories', [''], {stdio: 'inherit'});
  done();
}); 

// Regular Update Tasks
gulp.task('upgradeJSLib', (done) => {
  pump([
    gulp.src([
    'node_modules/lunr/lunr.js',
    'node_modules/macy/dist/macy.js'
    ]),
    gulpUglify(),
    gulp.dest(ASSET_JS)
  ]);

  gulp.src([ 
    'node_modules/smooth-scroll/dist/js/smooth-scroll.min.js',
    'node_modules/vanilla-lazyload/dist/lazyload.min.js'
  ]).pipe(gulp.dest(ASSET_JS));
  done();
});

// Build Tasks
gulp.task('stylus', function (done) {
  var postcss_plugins = [
    autoprefixer(),
    cssnano()
  ];
  gulp.src(['src/css/*.styl'])
    .pipe(stylus({
        compress: 'true',
    }))
    .on('error', function (err) {
      console.log(err.toString());
      this.emit('end');
    })
    .pipe(postcss(postcss_plugins))
    .pipe(gulp.dest('assets/css'));
  done();
});

gulp.task('jekyll', (done) => {
  const jekyll = child.spawn('bundle', [
    'exec',
    'jekyll',
    'serve',
    '--watch'
  ]);

  const jekyllLogger = (buffer) => {
    buffer.toString()
      .split(/\n/)
      .forEach((message) => log('Jekyll: ' + message));
  };

  jekyll.stdout.on('data', jekyllLogger);
  jekyll.stderr.on('data', jekyllLogger);
  done();
});

gulp.task('jekyll-build', (done) => {
  child.spawn('bundle', ['exec', 'jekyll', 'build']);
  done();
});

gulp.task('javascript', (done) => {
  gulp.src( JS_SRC )
    .pipe(babel())
    .pipe(babelMinify({
      mangle: {
        keepClassName: true
      }
    }))
    .pipe(gulp.dest(ASSET_JS));
  done();
});

gulp.task('minifyhtml', (done) => {
  gulp.src([
      SITE_ROOT + "*.html",
      SITE_ROOT + "**/*.html"
    ])
    .pipe(htmlmin({
      collapseWhitespace: true,
      removeComments: true
    }))
    .pipe(gulp.dest(SITE_ROOT + "./"));
  done();
});

gulp.task('dark-theme', (done) => {
  const darkConv = child.spawn('scripts/dark-mimic', [
    'src/css/style.styl',
    'src/css/night.styl'
  ], {stdio: 'inherit'});
  done();
});

gulp.task('start-browser', (done) => {
  child.spawn(BROWSER, ['127.0.0.1:4000'], { stdio: 'inherit' });
  done();
})

// Pipelined jobs
gulp.task('watch', () => {
  gulp.watch(['src/css/*.styl'], gulp.series('stylus'));
  gulp.watch('src/js/**/*.js', gulp.series('javascript'));
  gulp.watch('src/css/style.styl', gulp.series('dark-theme'));
});

gulp.task('build', gulp.series('jekyll-build', 'minifyhtml'));
gulp.task('deploy', gulp.series('generateTags', 'dark-theme', 'stylus', 'javascript', function (done) {
  console.log('[assistant] Ready to be shipped!');
  done();
}));
gulp.task('default', gulp.series('upgradeJSLib', 'stylus', 'javascript', 'jekyll',  'start-browser', 'watch'));
