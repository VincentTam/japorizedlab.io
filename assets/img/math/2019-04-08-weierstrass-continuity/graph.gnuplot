reset
set terminal svg
set output 'harmless-looking-graph.svg'
set border linewidth 1.5
unset key
set xrange [0:4]
set yrange [0:3]
set style line 1 lc rgb '#000000' lt 1 lw 2 #
f(x) = x>=0 && x<1 ? x : 1/0
g(x) = x>=2 && x<=3 ? x - 1 : 1/0
set label at 2,1 "" point pointtype 7 pointsize 2
set label at 1,1 "" point pointtype 6 pointsize 2
plot f(x) with lines ls 1,g(x) with lines ls 1
