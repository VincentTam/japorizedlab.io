---
layout: blog
pagination:
  enabled: true
  collection: math
title: Blog
---

## Personal Math Scribbles

Your [ticket](https://tex.japorized.ink) to my notes.

<small>PS: I do not guarantee that my proofs are absolutely correct. They are probably not. These notes here serve to be my personal stash of some of my exercises and revision that I do not wish to discard. Feel free to provide comments.</small>
