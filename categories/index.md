---
layout: default
title: Categories
permalink: /categories/
---

## <span class="fa fa-folder"></span> Categories

**[personal](personal.html)**  
**[siteupdate](siteupdate.html)**  
**[coding](coding.html)**  
**[technical](technical.html)**  
**[argumentative](argumentative.html)**  
